Rikofetcher - Modernizing store data fetching
============================================

Fetches store data from Japanese web stores. This is a rewrite of Miraifetcher
to clean up the increasingly complicated codebase by having a somewhat more
structured data output and also autoloading.

Requirements
------------

* PHP >= 7.1
* Dependencies are managed via Composer

Installation
------------

TODO


Usage
-----

```
use Damillora\Rikofetcher\Fetcher;

$result = Fetcher::fetchData("http://www.cdjapan.co.jp/product/NEOBK-2152109");

echo $result->toJson();
//{"success":true,"price":"1800","name":"Keyakizaka46 Nagahama Neru 1st Shashin Shu (Photo Book): Kokokara","image":"http:\/\/st.cdjapan.co.jp\/pictures\/l\/13\/35\/NEOBK-2152109.jpg?v=3","localshipping":null,"specials":null}
```
