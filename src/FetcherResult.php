<?php

namespace Damillora\Rikofetcher;

class FetcherResult {
	public $success = false;
	public $error;
	public $price = 0;
	public $name;
	public $image;
	public $localshipping = 0;
	public $specials;
	public function __toString() {
		if ($this->success) {
			return $this->price.';'.$this->name.';'.($this->image ?? '').(isset($this->localshipping) ? ';localshippingfee'.$this->localshipping : '').(isset($this->specials) ? ';'.$this->specials : '');
		} else {
			return $this->error;
		}
	}
	public function toJson() {
		return json_encode($this->toArray());
	}
	public function toArray() {
		$arr = (array) $this;
		if ($arr['success']) {
			unset($arr['error']);
		} else {
			unset($arr['price'], $arr['name'], $arr['image'], $arr['localshipping'], $arr['specials']);
		}
		return $arr;
	}
}
