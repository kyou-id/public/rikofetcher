<?php
namespace Damillora\Rikofetcher;

interface FetcherStore {
	public function retrieveHtml($url);
	public function toDom($body);
	public function processDom($dom,$url);
}
