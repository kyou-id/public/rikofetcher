<?php
namespace Damillora\Rikofetcher\Stores;

use Damillora\Rikofetcher\FetcherHtml;
use Damillora\Rikofetcher\FetcherDom;
use Damillora\Rikofetcher\FetcherString;
use Damillora\Rikofetcher\FetcherResult;

class Store_melonbooks_co_jp implements \Damillora\Rikofetcher\FetcherStore {
	public function retrieveHtml($url) {
		return FetcherHtml::body($url);
	}
	public function toDom($body) {
		return FetcherDom::normal($body);
	}
	public function processDom($dom,$url) {
		$result = new FetcherResult;
$price = $dom->find(".price",0)->plaintext ?? '';
if(!empty($price)){
$test = FetcherString::clws($dom->find("h1",0)->plaintext);
$price = str_replace("¥",'',$price);
$price = FetcherString::remove_comma($price);
$price = trim($price);
$image = $dom->find("img[itemprop=image]",0)->src;
$image = str_replace('//','https://',$image);
$result->success = true;
$result->price = $price;
$result->name = $test;
$result->image = $image;
$result->localshipping = 700;
}
else {
$result->success = false;
$result->error = 'soldout';
}
return $result;
	}
}
