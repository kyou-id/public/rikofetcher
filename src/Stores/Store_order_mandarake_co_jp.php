<?php
namespace Damillora\Rikofetcher\Stores;

use Damillora\Rikofetcher\FetcherHtml;
use Damillora\Rikofetcher\FetcherDom;
use Damillora\Rikofetcher\FetcherString;
use Damillora\Rikofetcher\FetcherResult;

class Store_order_mandarake_co_jp implements \Damillora\Rikofetcher\FetcherStore {
	public function retrieveHtml($url) {
		return FetcherHtml::body($url);
	}
	public function toDom($body) {
		return FetcherDom::normal($body);
	}
	public function processDom($dom,$url) {
		$result = new FetcherResult;
if(empty($dom->find(".operate .addcart",0))){
$result->success = false;
$result->error = 'soldout';
} else {
$test = FetcherString::clws($dom->find("h1",0)->plaintext);
$price = trim($dom->find("table",0)->children(2)->children(1)->plaintext);
$price = explode(' ',$price)[0];
$price = explode('円',$price)[0];
$price = FetcherString::sensible($price);
$image = $dom->find(".xzoom",0)->src;
$result->success = true;
$result->price = round($price);
$result->name = $test;
$result->image = $image;
}
return $result;
	}
}
