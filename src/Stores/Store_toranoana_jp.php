<?php
namespace Damillora\Rikofetcher\Stores;

use Damillora\Rikofetcher\FetcherHtml;
use Damillora\Rikofetcher\FetcherDom;
use Damillora\Rikofetcher\FetcherString;
use Damillora\Rikofetcher\FetcherResult;

class Store_toranoana_jp implements \Damillora\Rikofetcher\FetcherStore {
	public function retrieveHtml($url) {
		return FetcherHtml::body($url);
	}
	public function toDom($body) {
		return FetcherDom::normal($body);
	}
	public function processDom($dom,$url) {
		$result = new FetcherResult;
$in_stock = $dom->find('.instock');
	$out_of_stock = $dom->find('.outofstock');
	$pre_order = $dom->find('.futurerelease');
	$price = $dom->find('.Price_Dai',0)->plaintext;
	$price = str_sensible($price);
	$price = preg_replace("/[^0-9,.]/", "", $price );
	$name = $dom->find('.h2_itemDetail',0)->plaintext;
	$image = '';
	if(!empty($out_of_stock))
	{
		$result->success = false;
		$result->error = 'soldout';
	return $result;
	}
	$result->success = true;
	$result->price = $price;
	$result->name = $name;
	$result->image = $image;
	return $result;
	}
}
