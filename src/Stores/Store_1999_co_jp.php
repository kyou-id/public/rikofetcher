<?php
namespace Damillora\Rikofetcher\Stores;

use Damillora\Rikofetcher\FetcherHtml;
use Damillora\Rikofetcher\FetcherDom;
use Damillora\Rikofetcher\FetcherString;
use Damillora\Rikofetcher\FetcherResult;

class Store_1999_co_jp implements \Damillora\Rikofetcher\FetcherStore {
	public function retrieveHtml($url) {
		return FetcherHtml::body($url);
	}
	public function toDom($body) {
		return FetcherDom::normal($body);
	}
	public function processDom($dom,$url) {
		$result = new FetcherResult;
	$in_stock = $dom->find('.instock');
	$out_of_stock = strpos($dom->innertext,'売切') || strpos($dom->innertext,'Sold Out') ;
	$pre_order = $dom->find('.futurerelease');
	$price = $dom->find('.Price_Dai',0)->plaintext ?? $dom->find('#masterBody_trPrice',0)->children(2)->children(0)->children(0)->plaintext;
	$price = FetcherString::sensible($price);
	$price = preg_replace("/[^0-9,.]/", "", $price );
	$image = $dom->find('td[valign=middle] img',0)->src;
	$name = $dom->find('.h2_itemDetail',0)->plaintext;
	if($out_of_stock)
	{
		$result->success = false;
		$result->error = 'soldout';
		return $result;
	}
		$result->success = true;
		$result->price = $price;
		$result->name = $name;
		$result->image = FetcherString::rel2abs($image, $url);
		return $result;
	}
}
