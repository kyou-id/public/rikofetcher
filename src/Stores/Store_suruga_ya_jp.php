<?php
namespace Damillora\Rikofetcher\Stores;

use Damillora\Rikofetcher\FetcherHtml;
use Damillora\Rikofetcher\FetcherDom;
use Damillora\Rikofetcher\FetcherString;
use Damillora\Rikofetcher\FetcherResult;

class Store_suruga_ya_jp implements \Damillora\Rikofetcher\FetcherStore {
	public function retrieveHtml($url) {
		return FetcherHtml::bodyWithCookie($url, [ "adult" => '1', 'AUTH_ADULT' => '1', 'afg' => '0' ]);
	}
	public function toDom($body) {
		return FetcherDom::normal($body);
	}
	public function processDom($dom,$url) {
		$result = new FetcherResult;
$price = $dom->find(".red",-1)->plaintext ?? '';
if(!empty($price)){
$test = FetcherString::clws($dom->find("h2",0)->plaintext);
$price = str_replace("円",'',$price);
$price = str_replace("(税込)",'',$price);
$price = FetcherString::remove_comma($price);
$price = trim($price);
$image =  $dom->find('#imagecaption',0)->src;
$result->success = true;
$result->price = $price;
$result->name = $test;
$result->image = $image;
}
else {
$result->success = false;
$result->error = 'soldout';
}
return $result;
	}
}
