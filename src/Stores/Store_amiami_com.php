<?php
namespace Damillora\Rikofetcher\Stores;

use Damillora\Rikofetcher\FetcherHtml;
use Damillora\Rikofetcher\FetcherDom;
use Damillora\Rikofetcher\FetcherString;
use Damillora\Rikofetcher\FetcherResult;

class Store_amiami_com implements \Damillora\Rikofetcher\FetcherStore {
	public function retrieveHtml($url) {
		return FetcherHtml::dummy($url);
	}
	public function toDom($body) {
		return FetcherDom::crawler($body);
	}
	public function processDom($dom,$url) {
		$fetcherresult = new FetcherResult;
$finale = "";
$dom[0]->waitFor('.item-detail__image');
//$link = $dom[1]->selectLink('Confirm')->link();
//$dom[1] = $dom[0]->click($link);

$area = $dom[1]->filter('.item-detail__note')->text() ?? '';
$test = $dom[1]->filter('.btn-cart[style=""]');
$image = $dom[1]->filter(".item-detail__image img")->attr('src');
$test2 = $dom[1]->filter(".item-detail__section-title",0)->text() ?? '';
$result = $dom[1]->filter(".item-detail__price_selling-price",0)->text() ?? '';
if ($area == '*This product is not available in all areas.') {
$fetcherresult->success = false;
$fetcherresult->error = 'exclusive';
return $fetcherresult;
} else if(!empty($test)){
if($test->text() == 'Order Closed') {
$fetcherresult->success = false;
$fetcherresult->error = 'soldout';
return $fetcherresult;
}
$result = str_replace("JPY",'',$result);
$result = FetcherString::sensible($result);
$fetcherresult->success = true;
$fetcherresult->price = $result;
}else {
$fetcherresult->success = false;
$fetcherresult->error = 'soldout';
return $fetcherresult;
}
$fetcherresult->name = FetcherString::clws($test2);
$fetcherresult->image = $image ;
return $fetcherresult;
	}
}
