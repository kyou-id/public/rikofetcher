<?php
namespace Damillora\Rikofetcher\Stores;

use Damillora\Rikofetcher\FetcherHtml;
use Damillora\Rikofetcher\FetcherDom;
use Damillora\Rikofetcher\FetcherString;
use Damillora\Rikofetcher\FetcherResult;

class Store_animate_onlineshop_jp implements \Damillora\Rikofetcher\FetcherStore {
	public function retrieveHtml($url) {
		return FetcherHtml::body($url);
	}
	public function toDom($body) {
		return FetcherDom::normal($body);
	}
	public function processDom($dom,$url) {
		$result = new FetcherResult;
	$in_stock = $dom->find('.instock');
	$out_of_stock = $dom->find('.outofstock');
	$pre_order = $dom->find('.futurerelease');
	$price = $dom->find('.price',0)->plaintext;
	$price = explode("+",$price)[0];
	$price = FetcherString::sensible($price);
	$price = preg_replace("/[^0-9,.]/", "", $price );
	$name = $dom->find('.item_overview_detail h1',0)->plaintext;
	$image = $dom->find('.item_image_selected img',0)->src;
	if(!empty($out_of_stock))
	{
	$result->success = false;
	$result->error = 'soldout';
	return $result;
	}
	$result->success = true;
	$result->price = $price;
	$result->name = $name;
	$result->image = $image;
	$result->localshipping = 514;
	return $result;
	}
}
