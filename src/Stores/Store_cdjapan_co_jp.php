<?php
namespace Damillora\Rikofetcher\Stores;

use Damillora\Rikofetcher\FetcherHtml;
use Damillora\Rikofetcher\FetcherDom;
use Damillora\Rikofetcher\FetcherString;
use Damillora\Rikofetcher\FetcherResult;

class Store_cdjapan_co_jp implements \Damillora\Rikofetcher\FetcherStore {
	public function retrieveHtml($url) {
		return FetcherHtml::body($url);
	}
	public function toDom($body) {
		return FetcherDom::normal($body);
	}
	public function processDom($dom,$url) {
		if(strpos(trim($dom->find(".basic-info",0)->children(2)->children(1)->children(0)->innertext),"Sold Out") !== false){
			$result = new FetcherResult;
			$result->success = false;
			$result->error = 'soldout';
			return $result;
		} else {
			$test = $dom->find(".product_info h1",0)->plaintext;
			$image = $dom->find("img[itemprop=image]",0)->src ?? "";
			$yen = trim($dom->find(".price-info .price-jp-yen",0)->plaintext);
			$yen = str_replace('yen','',$yen);
			$result = new FetcherResult;
			$result->success = true;
			$result->price = $yen;
			$result->name = FetcherString::clws($test);
			$result->image = str_replace('//','http://',$image);
			return $result;
		}
	}
}
