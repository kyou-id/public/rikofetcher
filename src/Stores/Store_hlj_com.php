<?php
namespace Damillora\Rikofetcher\Stores;

use Damillora\Rikofetcher\FetcherHtml;
use Damillora\Rikofetcher\FetcherDom;
use Damillora\Rikofetcher\FetcherString;
use Damillora\Rikofetcher\FetcherResult;

class Store_hlj_com implements \Damillora\Rikofetcher\FetcherStore {
	public function retrieveHtml($url) {
		return FetcherHtml::body($url);
	}
	public function toDom($body) {
		return FetcherDom::normal($body);
	}
	public function processDom($dom,$url) {
		$result = new FetcherResult;
	$in_stock = $dom->find('.instock');
	$out_of_stock = $dom->find('.outofstock');
	$pre_order = $dom->find('.futurerelease');
	$price = $dom->find('.sale-price',0)->plaintext;
	$price = FetcherString::sensible($price);
	$price = preg_replace("/[^0-9,.]/", "", $price );
	$name = $dom->find('.product-title',0)->plaintext;
	$image = $dom->find('#gallery-area #thumb1',0)->href ?? "";
	$image = str_replace('//','https://',$image);
		$result->success = true;
		$result->price = $price;
		$result->name = $name;
		$result->image = $image;
	if(!empty($out_of_stock))
	{
		$result->specials = 'hljoos';
	}
	return $result;

	}
}
