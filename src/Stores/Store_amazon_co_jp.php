<?php
namespace Damillora\Rikofetcher\Stores;

use Damillora\Rikofetcher\FetcherHtml;
use Damillora\Rikofetcher\FetcherDom;
use Damillora\Rikofetcher\FetcherString;
use Damillora\Rikofetcher\FetcherResult;
use Damillora\Rikofetcher\Helper\AmazonOfferlist;

class Store_amazon_co_jp implements \Damillora\Rikofetcher\FetcherStore {
	public function retrieveHtml($url) {
		return FetcherHtml::body($url);
	}
	public function toDom($body) {
		return FetcherDom::purified($body);
	}
	public function processDom($dom,$url) {
		$result = new FetcherResult;
	$offerlist = $dom->find('#olp_feature_div .olp-padding-right a',0);
	$offerlist2 = $dom->find('#unqualifiedBuyBox a',0);
	$offerlist3 = $dom->find('.olp-new a',0);
	$test3 = FetcherString::sensible($dom->find('td[class=a-span12] .a-color-price',0)->innertext ?? '');
	$test5 = FetcherString::sensible($dom->find('.offer-price',0)->innertext ?? '');
	$test6 = FetcherString::sensible($dom->find('#unqualifiedBuyBox .a-color-price',0)->innertext ?? '');
	$test7 = FetcherString::sensible($dom->find('#priceblock_ourprice',0)->innertext ?? '');
	$test4 = FetcherString::clws($dom->find("h1",0)->plaintext);
	//$image = $dom->find("#landingImage",0)->outertext ?? null;
	$image = "";
	$finale = "";
	if(strpos($dom->innertext,'現在在庫切れです')){
	$result->success = false;
	$result->error = 'soldout';
	return $result;
	} else if(strpos($url, 'offer-listing') !== false){
		return AmazonOfferlist::processOfferlist($url);
	} else if(!empty($offerlist) || !empty($offerlist3) || !empty($offerlist2)){
	$offer = $offerlist ?? $offerlist3 ?? $offerlist2;
		$offerstring = FetcherString::rel2abs(html_entity_decode($offer->href),'https://amazon.co.jp');
		return AmazonOfferlist::processOfferlist($offerstring);
	} else {
	$test = $dom->find('.olp-padding-right',0);
	if(isset($test)) $test = $test->children(0)->href ?? '';
	$test2 = $dom->find('.olp-new',0);
	if(isset($test2)) $test2 = $test2->children(0)->href ?? '';
	$finale = "fetch-failed";	
	if (!empty($test3)) {
		$finale = $test3;
	} else if (!empty($test2)) {
		$finale = $test5;
	} else if (isset($test6)) {
		$finale = $test6;
	} else if (isset($test7)) {
		$finale = $test7;
	}
		$result->price = $finale;
		$result->name = $test4;
		$result->image = $image;
		$result->localshipping = 514;
	if ($finale == 'fetch-failed') {
		$result->success = false;
		$result->error = 'fetch-failed';
		return $result;
	} else {
		$result->success = true;
		return $result;
	}
}
	}
}
