<?php
namespace Damillora\Rikofetcher\Stores;

use Damillora\Rikofetcher\FetcherHtml;
use Damillora\Rikofetcher\FetcherDom;
use Damillora\Rikofetcher\FetcherString;
use Damillora\Rikofetcher\FetcherResult;

class Store_gamers_onlineshop_jp implements \Damillora\Rikofetcher\FetcherStore {
	public function retrieveHtml($url) {
		return FetcherHtml::body($url);
	}
	public function toDom($body) {
		return FetcherDom::normal($body);
	}
	public function processDom($dom,$url) {
		$result = new FetcherResult;
		$result->success = true;
		$price = $dom->find('.price',0)->plaintext;
		$price = floor(FetcherString::sensible(explode("+",$price)[0])*1.08);
		$result->price = $price;
		$name = $dom->find('h2',0)->plaintext;
		$result->name = $name;
		$image = $dom->find('#itemPhoto img',0)->src;
		$result->image = $image;
		$result->localshipping = 600;
		return $result;
	}
}
