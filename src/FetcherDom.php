<?php

namespace Damillora\Rikofetcher;

use Sunra\PhpSimple\HtmlDomParser;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Chrome\ChromeOptions;

class FetcherDom {
	public static function crawler($body) {
		$caps = DesiredCapabilities::chrome();
		$options = new ChromeOptions;
		$options->addArguments(array(
		    '--headless',
		  ));
		$caps->setCapability(ChromeOptions::CAPABILITY, $options);

		$client = \Symfony\Component\Panther\Client::createSeleniumClient("http://orpheus.kyou.local:4444/wd/hub",$caps);
		return  [$client, $client->request('GET',$body)]; 
	}
	public static function normal($body) {
		return  HtmlDomParser::str_get_html($body);
	}
	public static function purified($body) {
		$config = \HTMLPurifier_HTML5Config::createDefault();
		$purifier = new \HTMLPurifier($config);
		$config->set('Attr.EnableID', true);
		$config->set('HTML.Trusted', true);
		$config->set('CSS.Trusted', true);
		$html = $purifier->purify($body);
		return  HtmlDomParser::str_get_html($html);
	}

}
