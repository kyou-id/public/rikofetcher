<?php
namespace Damillora\Rikofetcher;
class FetcherJob {
	protected $module;
	protected $url;
	public function __construct($module) {
		$this->module = $module;
	}
	public function setUrl($value) {
		$this->url = $value;
	}
	public function retrieve() {
		$body = $this->module->retrieveHtml($this->url);
		$dom = $this->module->toDom($body);
		$data = $this->module->processDom($dom,$this->url);
		return $data;
	}
}
