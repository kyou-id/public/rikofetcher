<?php

namespace Damillora\Rikofetcher;

class FetcherHtml {
	public static function dummy($url) {
		return $url;
	}
	public static function body($url) {
		$client = new \GuzzleHttp\Client;
		$res = $client->request('GET',$url);
		return $res->getBody();
	}
	public static function bodyWithCookie($url,$cookie) {
		$client = new \GuzzleHttp\Client;
		$jar = \GuzzleHttp\Cookie\CookieJar::fromArray($cookie , parse_url($url,PHP_URL_HOST));

		$res = $client->request('GET',$url,  ['cookies' => $jar]);
		return $res->getBody();
	}
}
