<?php

namespace Damillora\Rikofetcher;

class FetcherString {
	public static function clws($str) {
		return trim(preg_replace('/\s+/', ' ', $str));
	}
	public static function rel2abs($rel, $base)
    {
        /* return if already absolute URL */
        if (parse_url($rel, PHP_URL_SCHEME) != '' || substr($rel, 0, 2) == '//') return $rel;

        /* queries and anchors */
        if ($rel[0]=='#' || $rel[0]=='?') return $base.$rel;

        /* parse base URL and convert to local variables:
         $scheme, $host, $path */
       	$comp = parse_url($base);
	$host = $comp['host'];
	$path = $comp['path'] ?? '';
	$scheme = $comp['scheme'];

        /* remove non-directory element from path */
        $path = preg_replace('#/[^/]*$#', '', $path);

        /* destroy path if relative url points to root */
        if ($rel[0] == '/') $path = '';

        /* dirty absolute URL */
        $abs = "$host$path/$rel";

        /* replace '//' or '/./' or '/foo/../' with '/' */
        $re = array('#(/\.?/)#', '#/(?!\.\.)[^/]+/\.\./#');
        for($n=1; $n>0; $abs=preg_replace($re, '/', $abs, -1, $n)) {}

        /* absolute URL is ready! */
        return $comp['scheme'].'://'.$abs;
    }
public static function remove_comma($str){
return str_replace(',','',$str);
}
public static function remove_nbsp($str){
return str_replace('&nbsp;','',$str);
}
public static function remove_yen($str){
$finale = str_replace('￥ ','',$str);
$finale = str_replace('円','',$finale);
$finale =  str_replace('￥','',$finale);
$finale =  str_replace('¥','',$finale);
return $finale;
}
public static function sensible($str){
$finale = FetcherString::remove_yen($str);
$finale = FetcherString::remove_comma($finale);
$finale = FetcherString::remove_nbsp($finale);
$finale = trim($finale);
return $finale;
}
public static function number($price){
return preg_replace("/[^0-9,.]/", "", FetcherString::sensible($price) );
}
}
