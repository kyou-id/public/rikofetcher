<?php
namespace Damillora\Rikofetcher;
class Fetcher {
	public static function fetchData($url) {
		$module = parse_url($url,PHP_URL_HOST);
		$module = str_replace("www.","",$module);
		$module = str_replace('.','_',$module);
		$module = str_replace('-','_',$module);
		$module = '\\Damillora\Rikofetcher\\Stores\\Store_'.$module;
		if (class_exists($module)) {
		$mod = new $module;
		$job = new FetcherJob($mod);
		$job->setUrl($url);
		$result = $job->retrieve();
		return $result;
		} else {
		$result = new FetcherResult;
		$result->success = false;
		$result->error = 'not-yet-supported';
		return $result;
		}
	}
}
