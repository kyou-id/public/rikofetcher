<?php
namespace Damillora\Rikofetcher\Helper;

use Damillora\Rikofetcher\FetcherHtml;
use Damillora\Rikofetcher\FetcherDom;
use Damillora\Rikofetcher\FetcherString;
use Damillora\Rikofetcher\FetcherResult;

class AmazonOfferlist {
	public static function processOfferlist($url) {
		$body = FetcherHtml::body($url);
		$dom = FetcherDom::normal($body);
		return AmazonOfferlist::interpretOfferlist($dom);
	}
	public static function interpretOfferlist($dom) {
$result = new FetcherResult;
$test4 = $dom->find("h1",0)->plaintext;
$primes = $dom->find('.olpOffer .a-icon-prime-jp');
$value = 0;
$image = $dom->find('#olpProductImage img',0)->src;
	if(count($primes) > 0){ 
		foreach ($primes as $prime) {
			$root = $prime->parent()->parent()->parent();
			$value = FetcherString::sensible($prime->parent()->parent()->find('.olpOfferPrice',0)->plaintext);
			$test4 = FetcherString::clws($test4);
			$result->success = true;
			$result->price = FetcherString::number($value);
			$result->name = $test4;
			$result->image = $image;
			return $result;
		}
	} else {
		$value = $dom->find('.olpOfferPrice',0);
		if(!$value) {
			$result->success = false;
			$result->error = 'soldout';
			return $result;
		}
		$test4 = FetcherString::clws($test4);
		$result->success = true;
		$result->price = FetcherString::sensible($value->innertext);
		$result->name = $test4;
		$result->image = $image;
		$result->localshipping = 514;
		return $result;
	}
	$test5 = $dom->find('#olpProductImage img',0)->src;
	$result->success = true;
	$result->price = $value;
	$result->name = $test4;
	$result->image = $image;
	return $result;
	}
}
